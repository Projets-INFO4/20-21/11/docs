# Node-Red name correspondance

Do not hesitate to modify if you found some errors or to propose other names more meaningful, or more practical
This document matches the data of the devices visible on ChirpStack to the names of variables usable on Grafana.
## QUART DEVICES :
- Waterstorage
- STM32_1
- STM32_2
- STM32_3 (not created yet)

## Devices usage :
- The Waterstorage device is designed to measure the filling of a water bag to hold water for watering the farm's greenhouses. It sends the data back via Lora and the frames can be seen on ChirpStack. The protocol used for the frames is Cayenne LPP.
- The STM32_x devices are boxes that take temperature and humidity measurements at three points for each box. They will be placed in one of the greenhouses. Three per greenhouse to improve accuracy. The data is sent back by Lora in the same way as for the waterstorage. The protocol used for the frames is Cayenne LPP.

## Data / Variable names

Correspondence between the fields of the frames and the fields in the influxDB database 

### DEVICE : Waterstorage

	- DigitalInput
		0 : Battery_WS (ws for waterstorage)
		1 : Distance_WS ou Remplissement_WS
	- TemperatureSensor
		2 : Temperature_WS

### DEVICE : STM32_1

	- Digital Input
		1 : Battery_B1 (for "boitier 1")
	- TemperatureSensor
		1 : Temperature1_B1
		2 : Temperature2_B1
		3 : Temperature3_B1
	- TemperatureSensor
		1 : Humidity1_B1
		2 : Humidity2_B1
		3 : Humidity3_B1

### DEVICE : STM32_2

	- DigitalInput
		1 : Battery_B2
	- TemperatureSensor
		1 : Temperature1_B2
		2 : Temperature2_B2
		3 : Temperature3_B2
	- TemperatureSensor
		1 : Humidity1_B2
		2 : Humidity2_B2
		3 : Humidity3_B2

### DEVICE : STM32_3

	- Digital Input
		1 : Battery_B3
	- TemperatureSensor
		1 : Temperature1_B3
		2 : Temperature2_B3
		3 : Temperature3_B3
	- TemperatureSensor
		1 : Humidity1_B3
		2 : Humidity2_B3
		3 : Humidity3_B3

authors:
  - Antoine Rivera, IESE
  - Hugo Prat-Capilla, INFO

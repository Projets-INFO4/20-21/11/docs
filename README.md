# Follow-up sheet: Projet 11 - ASAC
Julien Buisine, Theo Teyssier, Hugo Prat-Capilla

## Week 25/01/2021
- Following the course of Didier Donsez on LoRa
## Week 01/02/2021
- Meeting with M.Palix, for a global presentation of the project
## Week 08/02/2021
- Installation of the different required software for the project
- Beginning of the formation on the different technology beginning by Docker
- Reading the document of the previous group in order to understand the project
## Week 15/02/2021
- Trying to build the project (it was legacy code so not very useful)
- Learning Docker and a little bit of node js
- Writing a list of goals and the resources that we can use for learning
## Week 22/02/2021
- Building the docker of the project
- Writing basic documentation around docker
- Learning to use node-red, Graphana and influxDB
- Beginning of the organization on Gitlab, using Issus and milestone
- Meeting with the IESE teams and an ST engineer to discuss how we can work with them
## Week 01/03/2021
- Finishing the GitLab organization
- Working on a first milestone
   - Reorganising the codebase of the project
   - Writing a new Readme for the project documenting how to set-up everything
- Meeting with M.Palix to discuss the project and our work
- Researching on the Graphana save error
- Fixing the .gitignore of the project
- Trying to put in place CI test
## Week 08/03/2021
- Intermediate presentation
## Week 15/03/2021
- Update of the wiki page
- Study of the Thingsboard solution
- Modification of the water tank nodered to decode wefts

## Week 22/03/2021
- Success of the save system
- Update of the nodered and documentation about it

## Week 29/03/2021
- Meeting with Mr Palix
- Start to make the report

## Week 05/04/1999
- Make the report 
- Prepare the defense of our project
